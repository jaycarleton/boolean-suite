//CODE FOR FIND-OUTPUT PAGE, given expression and list of true variables



//Standardizes the various possible input notations of a user to php understand boolean syntax
//@param $expr: the expression to standardize
//@retun $expr: the newly standardized expression
function fixformat($expr)
{

    //Fix the XORS
    $xor=array('XOR','Xor ','xor');
    $expr=str_replace($xor,'^',$expr);
    
    //Fix the ORs
    $or=array('+','or','OR','Or');
    $expr=str_replace($or,'||',$expr);
    
    //Fix the ANDS
    $and=array('.','AND','and','And');
    $expr=str_replace($and,'&&',$expr);
    
    //Fix the parenthases
    $expr=str_replace('[','(',$expr);
    $expr=str_replace(']',')',$expr);
    
    //Fix the Spaces
    $expr=str_replace(' ','',$expr);

    return $expr;//return the standardized expression
}


//Function to replace all multiple spaces with single spaces so the varlist function has no empty elements
//Recursion could be used, however this is faster for the server to process
//@param $string: the string that may contain multiple spaces
//@return $string: the newly modified string, with no duplicate spaces
function single($string)
{
    $length=strlen($string);//start off at max possible value
    for($i=$length;$i>1;$i--)//decrement each time
    {
        if(strpos($string,str_repeat(' ', $i))!==false)//if X number of spaces exist                                 concurrently
        {
            //echo $string."\n"; for testing only
            $string=str_replace( (str_repeat(' ',$i)),' ',$string);//replace the X spaces with a single space
        }
    }
    return $string;//return the modified string, with no multiple spaces
}



//Function to creat an array of individual variables inside the expression, one of each element only
//@param $expr: the expression to create a list of variables for
//@return $x: the list of unique variables in the expression
function varlist($expr)
{
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('||','&&','^',')','(','[',']','\n','\t','!');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    $stack = explode(' ', $i);//divide string into array, seperate at each space
    
    //remove duplicates
    $stack= array_unique($stack);//make array unique
    foreach($stack as $key=>$value)//remove empty element entries
    {
        if ($value=="")//if element is blank
        {
            unset($stack[$key]);//remove from list
        }
    }
    
    $x=array();//create blank array for answer list
    
    //fix the indices of the list by copying the current one, element-by-element
    foreach($stack as $value)
    {
        array_push($x,$value);//push onto new array
    }
    
    return $x;//returns array of variables 
}

//Creates an arraylist of values that are set true manually by the user
//@param $expression: the expression for which to find true variables for
//@return $stack: the array of true variables
function findtrues($expr)
{
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('.',',',';',' ');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    $stack = explode(' ', $i);
    
    //remove duplicates
    $stack= array_unique($stack);
    //var_dump($stack); for testing only
            
    return $stack;//returns array of variables 
}



//Function evaluates the boolean result of expression where the given list of parameters is true
//@param $expr: the expression to evaluate
//@param $trues: the list of variables set true in the expression evaluation
function result($expr,$trues)
{   
    //replace the variables with their literal boolean values
    $e=str_replace($trues,'true',$expr);//replace the trues
                                        //variables no longer exist in expression
                                        //only false remain
    $e=str_replace(varlist($expr),'false',$e);//all else set false
    
    //Eval
    eval("\$result=$e;");//evaluate string as boolean expression
    return $result;//return boolean result of expression
}

//function to find the output of a boolean expression, given a list of true variables
//@param $expr: the expression to evaluate
//@param $list: the list of variables that are set to true for the expression
//@return $output: the boolean value of the expression, when certain variables are set true
function evaluate($expr,$list)
{
    $expr=fixformat($expr);//standardize format of expression
    
    //check to ensure the expression is not empty
    if(count(varlist($expr))===0)//if the expression contains no variables
    {
        return "The expression must contain at least one valid input.";//error
    }
    
    $trues=findtrues($list);//create standardized list of true variables based on list
    
    //ensure that the list of true variables does not contain any blank elements
    //this will allow the function to operate when NO variables are set true
    foreach($trues as $key=>$variable)
    {
        if ($variable==="")
        {
            $trues=array();
        }
    }
    
    $varlist=varlist($expr);//create as list of expression variables to compare with the list
    
    //test to ensure each element of the list is actually in the expression
    foreach($trues as $variable)//for every element in the given list
    {
        if (! in_array($variable,$varlist))//if not also in the expression
        {
            return "The value '".$variable."' is not in the expression.";//error
        }
        //else nothing
    }
    $output=result($expr, $trues);//evaluate the result of the expression
    
    return $output;//return the final value of the expression
}


//EXAMPLE MAIN PROGRAM

//$expr=' ';//example string
//$list='';//example list

//echo evaluate($expr,$list);