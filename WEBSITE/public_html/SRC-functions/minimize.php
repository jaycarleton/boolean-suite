
//applies the identity law to an expression
//reduces from term.term->term
//also reduces from term+term->term
//does not reduces term.!term or term+!term
//assumes expression has already been formatted
//@param $expr: the formatted expression to apply the law to
//@return $expr: the newly reduced expression
function applyIL($expr)
{
    //create a list of variables for the expression
    //$varlist=varlist($expr);
    $varlist=array('a','b');
    
    $delimiter=array(" ","\0");//remove all spaces and null key, to ensure length
    
    //create expression seperating sections of expression by AND gates
    $reduceANDs=explode("&&",$expr);
    
    //remove spacing from each section, so as to properly determine number of          literals
    foreach($reduceANDs as $key=>$section)
    {
        $reduceANDs[$key]=str_replace($delimiter,"",$section);//remove all spacing to ensure only literals are counted
    }
    
    //ensure that no duplicate elements exist within the array
    $reduceANDs=array_unique($reduceANDs);
    
    //create array of special singular variables that need to be added at the end
    $singles=array();
    
    //remove duplicate ANDed terms now
    foreach($reduceANDs as $key1=>$section1)
    {
        //compare against every variable
        foreach($varlist as $var)
        {
            //if the section is just a variable
            if($section1===$var)
            {
                array_push($singles,$var);//add to special list of singular inputs
                
                foreach($reduceANDs as $key2=>$section2)//compare against sections
                {
                    
                    if($section1===$section2)//if comparing same section
                    {
                        //do nothing, you are comparing the same term
                    }
                    else//not same term
                    {
                        if($section2==="!".$section1)//!var is in $section2
                        {
                            //do nothing, cannot apply IL to !a.a or !a+a
                        }
                        else//does not include !term
                        {
                            //remove both types of possible term instances
                            if(strpos("||".$section1,$section2)!==FALSE)
                            {
                                $reduceANDs[$key2]=str_replace("||".$section1,
                                "",$section2);
                                
                                $reduceANDs[$key2]=str_replace($delimiter,"",
                                    $reduceANDs[$key2]);
                                
                                //if reduced to only single instance
                                if($reduceANDs[$key2]===$var)
                                {
                                    array_push($singles,$var);//push instance to singles
                                }
    
                            }
                            else//other form
                            {
                                $reduceANDs[$key2]=str_replace($section1."||",
                                "",$section2);
                                  
                                $reduceANDs[$key2]=str_replace($delimiter,"",
                                $reduceANDs[$key2]);
                                
                                if($reduceANDs[$key2]===$var)//reduced to single instance
                                {
                                    array_push($singles,$var);//push to singles
                                }
                            }
                        }
                    }
                }
                $reduceANDs=array_unique($reduceANDs);//make singles have no duplicates
            }
            elseif ($section1==="!".$var)//section is ONLY !term
            {
                array_push($singles,$section1);//add to special list of singular inputs
  
                foreach($reduceANDs as $key2=>$section2)//compare against other sections
                {
                    if($section1===$section2)
                    {
                        //do nothing, you are comparing the same term
   
                    }
                    else//not same term
                    {
                        if(strpos("||".$section1,$section2)!==FALSE)
                        {
                            $reduceANDs[$key2]=str_replace("||".$section1,
                            "",$section2);
                            
                            $reduceANDs[$key2]=str_replace($delimiter,"",
                            $reduceANDs[$key2]);
                            
                            if($reduceANDs[$key2]==="!".$var)
                            {
                                $temp="!".$var;
                                array_push($singles,$temp);
                            }
                        }
                        else
                        {
                            echo "NOW REPLACE\n";
                            $reduceANDs[$key2]=str_replace($section1."||",
                            "",$section2);
                            
                            $reduceANDs[$key2]=str_replace($delimiter,"",
                            $reduceANDs[$key2]);
                            
                            if($reduceANDs[$key2]==="!".$var)
                            {
                                $temp="!".$var;
                                array_push($singles,$temp);
                            }
                            
                            echo $reduceANDs[$key2]."\n";
                        }
                        
                    }
                }
                
                
            }
        }
    }
    
    //make array unique, remove duplicates
    $reduceANDs=array_unique($reduceANDs);
    
    
    
    
    
     
    $expr="";//restart the expression
    
    //now recreate the original expression, but this time without the duplicate variables
    foreach($reduceANDs as $section)
    {  
            $expr.=$section."&&";
    }
    
    $expr[strlen($expr)-1]="";
    $expr[strlen($expr)-2]="";
    
   //at this point, all terms of  form a.a have been minimized to a.a->a
    
    
    //create expression seperating sections of expression by AND gates
    $reduceORs=explode("||",$expr);
    
    //remove spacing from each section, so as to properly determine number of          literals
    foreach($reduceORs as $key=>$section)
    {
        
        
        $reduceORs[$key]=str_replace($delimiter,"",$section);//remove all spacing to ensure only literals are counted
    }
    
    //ensure that no duplicate elements exist within the array
    $reduceORs=array_unique($reduceORs);
    
    var_dump($reduceORs);
    
    //remove duplicate ANDed terms now
    foreach($reduceORs as $key1=>$section1)
    {
        
        foreach($varlist as $var)
        {
           
            if($section1===$var)
            {
                //echo "HIT\n";
                foreach($reduceORs as $key2=>$section2)
                {
                    //echo "SECTION 2 is". $section2."\n";
                    if($section1===$section2)
                    {
                        //do nothing, you are comparing the same term
                       // echo"DO NOTHING,SAME THING\n\n";
                    }
                    else//not same term
                    {
                        if($section2==="!".$section1)//!var is in $section2
                        {
                            //do nothing
                          //  echo "ERROR";
                        }
                        else
                        {
                            if(strpos("&&".$section1,$section2)!==FALSE)
                            {
                                $reduceORs[$key2]=str_replace("&&".$section1,
                                "",$section2);
                                
                                $reduceORs[$key2]=str_replace($delimiter,"",
                                    $reduceORs[$key2]);
                                
                                if($reduceORs[$key2]===$var)
                                {
                                    array_push($var,$singles);
                                }
    
                            }
                            else
                            {
                                $reduceORs[$key2]=str_replace($section1."&&",
                                "",$section2);
                                  
                                $reduceORs[$key2]=str_replace($delimiter,"",
                                $reduceORs[$key2]);
                                
                                if($reduceORs[$key2]===$var)
                                {
                                    array_push($var,$singles);
                                }
                            }
                        }
                    }
                }
                var_dump($reduceORs);
                $reduceORs=array_unique($reduceORs);
            }
            elseif ($section1==="!".$var)
            {
                echo"SECTION 1 is ".$section1."\nVAR is "."!".$var."\n";
                echo "HIT\n";
                foreach($reduceORs as $key2=>$section2)
                {
                    echo "SECTION 2 is". $section2."\n";
                    if($section1===$section2)
                    {
                        //do nothing, you are comparing the same term
                        echo"DO NOTHING,SAME THING\n\n";
                    }
                    else//not same term
                    {
                        echo "NOT NOTHING\n";
                        if(strpos("&&".$section1,$section2)!==FALSE)
                        {
                            $reduceORs[$key2]=str_replace("&&".$section1,
                            "",$section2);
                            
                            $reduceORs[$key2]=str_replace($delimiter,"",
                            $reduceORs[$key2]);
                            
                            if($reduceORs[$key2]==="!".$var)
                            {
                                $temp="!".$var;
                                array_push($temp,$singles);
                            }
                        }
                        else
                        {
                            echo "NOW REPLACE\n";
                            $reduceORs[$key2]=str_replace($section1."&&",
                            "",$section2);
                            
                            $reduceORs[$key2]=str_replace($delimiter,"",
                            $reduceORs[$key2]);
                            
                            if($reduceORs[$key2]==="!".$var)
                            {
                                $temp="!".$var;
                                array_push($temp,$singles);
                            }
                            
                            echo $reduceORs[$key2]."\n";
                        }
                        
                    }
                }
                
                
            }
        }
    }
    
    $reduceORs=array_unique($reduceORs);
    
    $singles=array_unique($singles);
    
    
    
    
     
    $expr="";//restart the expression, for the final answer
    
    //now recreate the original expression, but this time without the duplicate variables
    foreach($reduceORs as $section)
    {  
            $expr.=$section."&&";
    }
    $expr[strlen($expr)-1]="|";
    $expr[strlen($expr)-2]="|";
    
    var_dump($singles);
    
    foreach($singles as $single)
    {
         $expr.=$single;
    }
    
    return $expr;

}

echo applyIL("a||a || b&&b");