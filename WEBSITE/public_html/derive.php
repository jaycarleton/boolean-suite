<!DOCTYPE html>

<?php
  $numInputs = $_POST["numInputs"];
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Derive</title>
  </head>

  <body>
    <?php include("includes/navbar.html");?>

    <div class="container-fluid">

      <div class="row">
        
        <div class="col-10 col-sm-10 col-lg-10">
          <form method="post" action="<?php echo $PHP_SELF;?>">
            Select number of inputs: 
            <div class="btn-group">
              <button type="submit" value="2" name="numInputs" class="btn btn-default">2</button>
              <button type="submit" value="3" name="numInputs" class="btn btn-default">3</button>
              <button type="submit" value="4" name="numInputs" class="btn btn-default">4</button>
            </div>
          </form><br />

          <?php if (isset($_POST['numInputs'])) {
          ?>
            <div class="table-responsive">
              <table class="table table-striped">
              
                <?php if($numInputs==2) {
                ?>
                  <tr>
                    <td><input type="text" class="form-control" placeholder="Var1"></td>
                    <td><input type="text" class="form-control" placeholder="Var2"></td>
                    <td>Value</td>
                  </tr>
                  <tr><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                <?php 
                } else if($numInputs==3){ 
                ?>
                  <tr>
                    <td><input type="text" class="form-control" placeholder="Var1"></td>
                    <td><input type="text" class="form-control" placeholder="Var2"></td>
                    <td><input type="text" class="form-control" placeholder="Var3"></td>
                    <td>Value</td>
                  </tr>
                  <tr><td>0</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                <?php 
                } else {
                ?>
                  <tr>
                    <td><input type="text" class="form-control" placeholder="Var1"></td>
                    <td><input type="text" class="form-control" placeholder="Var2"></td>
                    <td><input type="text" class="form-control" placeholder="Var3"></td>
                    <td><input type="text" class="form-control" placeholder="Var4"></td>
                    <td>Value</td>
                  </tr>
                  <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[0]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>0</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[1]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[2]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>0</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[3]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[4]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[5]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[6]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[7]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[8]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[8]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[8]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[9]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[9]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[9]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[10]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[10]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[10]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[11]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[11]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[11]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>0</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[12]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[12]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[12]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[13]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[13]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[13]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>1</td><td>0</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[14]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[14]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[14]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                  <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[15]" id="inlineRadio1" value="option1" checked="">0
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[15]" id="inlineRadio2" value="option2">1
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="inlineRadioOptions[15]" id="inlineRadio3" value="option3">d
                    </label>
                  </td></tr>
                <?php
                }?>
                  
              </table>
            </div>
          <?php
          }?>


        </div> <!-- col -->

        <div class="col-2 col-sm-2 col-lg-2">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
            <span class="glyphicon glyphicon-question-sign"></span>
          </button>

          <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Help</h4>
                </div>
                <div class="modal-body">
                  <p>Enter an expression in the input box, then press the go button. The simplified result of the equation, based on the Quine-McClusky algorithm is shown in the output box.Formatting for expressions allows all standardized notations for logical operators.</p>
                  <p>ANDs can be expressed with AND,And,.,and</p>
                  <p>ORS can be expressed with +, or, OR, Or</p>
                  <p>XORS can be expressed by XOR, Xor, xor,</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      
      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>