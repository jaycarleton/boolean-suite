<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Contact</title>
  </head>

  <body>
    <?php include("includes/navbar.html");?>

    <div class="container-fluid">

      <form role="form">
        <fieldset disabled><div class="form-group">
          <label for="inputName1">Name</label>
          <input type="name" class="form-control" id="inputName1" placeholder="Enter name">
        </div>
        <div class="form-group">
          <label for="inputEmail1">Email address</label>
          <input type="email" class="form-control" id="inputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="inputQuestion1">Question</label>
          <input type="question" rows="10" class="form-control" id="inputQuestion1" placeholder="Question">
          <!--<textarea class="form-control" rows="10"></textarea>-->
        </div>  
        <button type="submit" class="btn btn-default">Submit</button></fieldset>
      </form>

      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>