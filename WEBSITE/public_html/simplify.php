<!DOCTYPE html>

<?php
  $expr = $_POST["expr"];

  //Standardizes the various possible input notations of a user to php understand boolean syntax
  function fixformat($expr)
  {
    //Fix the XORS
    $xor=array('XOR','Xor ','xor');
    $expr=str_replace($xor,'^',$expr);
    
    //Fix the ORs
    $or=array('+','or','OR','Or');
    $expr=str_replace($or,'||',$expr);
    
    //Fix the ANDS
    $and=array('.','AND','and','And');
    $expr=str_replace($and,'&&',$expr);
    
    //Fix the parenthases
    $expr=str_replace('[','(',$expr);
    $expr=str_replace(']',')',$expr);
    
    //Fix the Spaces
    $expr=str_replace(' ','',$expr);
    
    //Echo just to check the above "fixing" of input types worked
    //echo $expr; testing only
    return $expr;
  }

  //Function to remove all multiple spaces with single spaces so the varlist function has no empty elements
  //Recursion could be used, however this is faster for the server to process
  function single($string)
  {
    $length=strlen($string);//start off at max possible value
    for($i=$length;$i>1;$i--)//decrement each time
    {
      if(strpos($string,str_repeat(' ', $i))!==false)//if X number of spaces exist                                 concurrently
      {
        //echo $string."\n"; for testing only
        $string=str_replace( (str_repeat(' ',$i)),' ',$string);//replace the X spaces with a single space
      }
    }
    return $string;
  }

  //Function to creat an array of individual variables inside the expression, one of each element only
  function varlist($expr)
  {
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('||','&&','^',')','(','[',']','\n','\t','!');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    
    $stack = explode(' ', $i);
    //remove duplicates
    $stack= array_unique($stack);
    //var_dump($stack); for testing only
    foreach($stack as $key=>$value)
    {
      if ($value=="")
      {
        unset($stack[$key]);
      }
    }
    $x=array();
    foreach($stack as $value)
    {
      array_push($x,$value);
    }
    
    return $x;//returns array of variables 
  }

  function ismax($i)//test for 111 at end of loop
  {
    foreach($i as $value)
    {
      if($value===0)
      {
        return false;
      }
    }
    return true;
  }

  //Function evaluates the boolean result of expression where the given list of parameters is true
  function result($expr,$trues)
  {    
    $e=str_replace($trues,'true',$expr);
    $e=str_replace(varlist($expr),'false',$e);
    //echo $e;//for testing only
    
    //Eval
    eval("\$result=$e;");
    //eval("\$result=\"$e\";");
    return $result;
  }

  function inc($a)//where a is an array
  {
    $length=count($a);
    //assume not given all 1's
    if($a[$length-1]==0)
    {
      $a[$length-1]=1;
      //echo "last is zero"; testing only
      return $a;
    }
    //else
    $i=$length-1;
    while($a[$i]==1)
    {
      $a[$i]=0;
      $i--; 
    }
    if($a[$i]==0)
    {
      $a[$i]=1;
    }
    return $a;
  }

  ////
  ////
  ////
  ////
  ///
  ///NEW FUNCTIONS////

  //the binary representation of the list of variables that make the expression true
  class Term
  {
    //instance variables
    public $array;//the binary representation of the variables
    public $covered;//the array of minterms that cover this term
    public $care;//determines if the term is a "dont-care" term, or not
    
    //constructor
    //@param $array: array of 0s,1's that represent binary values of set variables
    //@param $care:determines whether the value is a don't-care term
    public function __construct($array,$care)
    {
      $this->array=$array;//set array
      $this->care=$care;//set care/ don't-care status
      $this->covered=array();//the array of minterms that cover this term
    }   
    
    //creates a string representation of the term, with respect to the expression variables
    //@param $varlist: the list of expression variables
    //@return $string: the string representation of the term
    public function toString($varlist)
    {
      $string="+";//blank string representation, only plus sign to show OR
      
      if($this->care==false)//if the term is a "dont-care"
      {
        return "";//do nothing, as it is not required to be shown in final answer
      }
      
      foreach($this->array as $key=>$val)
      {
        if($val===1)//true
        {
          $string.=$varlist[$key];//add current element of term
        }
        elseif($val===0)//false
        {
          $string.="!";
          $string.=$varlist[$key];//add NOT of current element of term
        }
      }
      $string.=" ";//add a space for to ease reading
      return $string;
    }
    
    //function to determine whether the term is covered by a minterm
    public function isCovered()
    {
        if(count($this->covered)===0)//if list of covering minterms is empty
        {
            return false;//then the term has no covering minterm
        }
        //else
        return true;//term has at least one covering minterm
    }
    
    //cover the term with a minterm
    //@param $minterm: the covering minterm
    public function getCoveredBy($minterm)
    {
      array_push($this->coveredBy,$minterm);//push onto array of covering minterms
    }  
  }

  //the special term that covers a multitude of other terms
  class Minterm
  {
    //instance variables
    public $array;//the binary representation of the covered terms
    public $covers;//the list of terms this minterm covers
    public $covered;//determines if the minterm is covered by a simpler minterm
    
    //constructor
    //@param $array1: the first array to base the minterm off of
    //@param $array2: the second array to base the minterm off of
    public function __construct($term1,$term2)
    {
      $size=count($term1->array);//set size of array, so it can be set
      
      //set the array contents now
      for($i=0;$i<$size;$i++)
      {
        if($term1->array[$i]===$term2->array[$i])//if both arrays match at this index
        {
          $this->array[$i]=$term1->array[$i];//same value for minterm
        }
        else//if the array values differ at this index
        {
          $this->array[$i]='x';//put an x to show both values
        }
      }
      $this->covers=array();//create the array for covered terms
      $this->covered=false;
    }
    
    //function to cover a given term
    //@param $term: the term to cover
    public function coverNew($term)
    {
      array_push($this->covers,$term);//cover this term
      array_push($term->covered,$this);//add to terms list of covering minterms
    }
    
    //creates a string representation of the term, with respect to the expression variables
    //@param $varlist: the list of expression variables
    //@return $string: the string representation of the term
    public function toString($varlist)
    {
      $string="+";//blank string representation, only plus sign to show OR
      
      foreach($this->array as $key=>$val)
      {
        if($val===1)//true
        {
          $string.=$varlist[$key];//add current element of term
        }
        elseif($val===0)//false
        {
          $string.="!";
          $string.=$varlist[$key];//add NOT of current element of term
        }
      }
      $string.=" ";//add a space for to ease reading
      return $string;
    }
    
    //returns the number of literals/ variable values in the minterm
    //@return $literals: the integer value of the number of literals in the expression
    function literals()
    {
      $literals=0;//start the value off at 0
      
      foreach($this->array as $element)
      {
        if($element === 0 || $element === 1)//if the current element is not 'x'
        {
          $literals+=1;//increment the number of literals 
        }
        //else do nothing
      }
      return $literals;//return the number of literals in the minterm
    }
  }

  //creates an array of terms based on the current expression
  //@param $expr: the expression on which terms that relate to true values are derived from
  //@ return $terms: an array of terms which correspond to true expression evaluations
  function createTerms($expr)
  {
    $list=varlist($expr);//list of expression variables
    $terms=array();//blank array of answer terms   
    $i=array();//array for current binary representation of variable values
    $trues=array();//list of current variable names that are set as 1 in $i
    $size=count(varlist($expr));//size of variable list, needed to use inc properly
    
    //create empty binary representation array of variables, with same size as varlist
    for($it=0;$it<$size;$it++)
    {
      array_push($i,0);//create empty/unset member
    }
    //now all variables are set to false(start position)
    
    while(true)//loop through all possible expression values
    {
      $trues=array();//clear string representation of true-set variables

      foreach($i as $key=>$value)//create array of trues based on current i list
      {
        if ($value===1)
        {
          array_push($trues,$list[$key]);//push currently set variables onto trues
        }
      }

      //add to list of terms
      if (result($expr,$trues))//if true at these variables settings
      {             
        $new=new Term($i,true);//push current values of variables onto array
        array_push($terms,$new);//not same expression
      }

      //if done
      if(ismax($i))//we got to the last, return the list of terms
      {
        return $terms;//return list of terms
      }
      $i=inc($i);//increment the binary representation of the variable values
    }
  }

  function differByOne($array1,$array2)
  {
    $different=false;//start off with no difference yet
    $size=count($array1);//size of arrays, could have been either
    
    for($i=0;$i<$size;$i++)
    {
      if($array1[$i]!==$array2[$i])
      {
        if($different)//if already differing by one digit, no minterm possible
        {
          return false;
        }
        //else set to different
        $different=true;//minterm now/stil possible
      }
    }
    return $different;//minterm is possible
  }

  function createMinterms($terms)
  {
    $changed=false;//status of occured changes
    $minterms=array();//empty array of covering minterms
    
    //create list of minterms
    while(!$changed)//keep going until no more changes occur
    {
      $changed=false;//no change occured on current loop
      
      //check each term against every other term to see if minterm possible
      foreach($terms as $val1)
      {
        foreach($terms as $val2)
        {
          if( $val1->array===$val2->array)//if checking same term
          {
            //do nothing
          }
          //else
          if(differByOne($val1->array,$val2->array))// if minterm possible
                                                    //already ensures index is different
          {
            $new=new Minterm($val1,$val2);//create new minterm to
            array_push($minterms,$new);//put minterm onto array
            $changed=true;//a change was made, more revision may be needed
          }
        }
      }   
    }//end of while loop
    return $minterms;
  }

  //makes it such that only one instance of each item is in the given list
  //cannot use array_unique for complex objects(eg Term, or Minterm)
  //works for either Terms or Minterms
  //@param $terms: the list of Terms, or Minterms to make unique
  //@param $varlist: the list of variables for the expression, needed for toString
  //@return $terms: the now unique list of Terms, or Minterms
  function uniqueTerms($terms,$varlist)
  {
    $strings=array();
    foreach($terms as $key=>$val)
    {
      if(!in_array($val->toString($varlist),$strings))
      {
        array_push($strings,$val->toString($varlist));
      }
      else
      {
        unset($terms[$key]);
      }
    }
    return $terms;
  }

  //returns the index/number of ones in each Minterm
  //@param $term: the Minterm, to get the index of
  //@return $index: the number of one in each Minterm
  function indexOf($term)
  {
    $index=0;//the index starts at 0
    
    foreach($term->array as $val)//check every element
    {
      if($val===1)//if the element is 1
      {
        $index+=1;//increase the index
      }
    }
    return $index;//return the number of ones in the Minterm
  }

  //gives the maximum index, or number of 1s for a Minterm in the list
  //@param $minterms: the list of minterms to return the max index for
  //@return $max: the maximum index of the array
  function maxIndex($minterms)
  {
    $max=0;//maximum index of array
    foreach($minterms as $minterm)//check every element
    {
      if(indexOf($minterm)>$max)//if current element has higher index
      {
        $max=indexOf($minterm);//index of array becomes index of element
      }
    }
    return $max;//return the index of the array
  }

  //gives the minimum index, or number of 1s for a Minterm in the list
  //@param $minterms: the list of minterms to return the min index for
  //@return $min: the minimum index of the array
  function minIndex($minterms)
  {
    $min=maxIndex($minterms);//the minimum index of the array starts at its max possible value
    
    foreach($minterms as $minterm)
    {
      if(indexOf($minterm)<$min)//if index of element is less than index of array
      {
        $min=indexOf($minterm);//index of array becomes index of element
      }
    }
    return $min;//return index of array
  }

  function indexMinterms($minterms)
  {
    $index=array();
    $max=maxIndex($minterms);
    $min=minIndex($minterms);
    
    for($i=$min;$i<=$max;$i++)
    {
      $index[$i]=array();
    }
    
    foreach($minterms as $minterm)
    {
      $currentIndex=indexOf($minterm);
      array_push($index[$currentIndex],$minterm);
    }
    return $index;
  }

  function minimizeMinterms($minterms)
  {
    $index=indexMinterms($minterms);
    
    $max=maxIndex($minterms);
    $min=minIndex($minterms);
    
    $size=count($minterms[0]);
    for($i=1;i<$size;$i++)
    {   
      for($j=$max;$j>1;$j++)
      {
        if(count($index[$j])===0 || count($index[$j-1]===0))
        {
            //do nothing
        }
        else
        {
          foreach($index[$j] as $big)
          {
            foreach($index[$j-1] as $small)
            {
              if(differByOne($big,$small))
              {
                $new=new Minterm($big,$small);
                array_push($minterms,$new);
                $big->covered=true;
                $small->covered=true;
              }
              //else nothing
            }
          }
        }
      }
      
      foreach($minterms as $key=>$value)
      {
        if($value->covered)
        {
            unset($minterms[$key]);
        }
      }
      $minterms=uniqueTerms($minterms);
      $index=indexMinterms($minterms);
    }
    return $minterms;
  }   

  //function to return a 2D array, containing all viables minterm comparisons
  //ensures all pairs have index differing by one
  //@param $minterms: the list of minterms to create the viable comparison array for
  //@return $viables: the list of viables indexes to compare minterms to in the array
  function viableIndexes($minterms)
  {
    $viables=array();//create array for indexes of viable minterm comparison
    
    $fullIndex=array();//create temorary array for evey index in the list of minterms
    
    //add the index of every minterm in the array to $fullIndex
    foreach($minterms as $minterm)
    {
      array_push($fullIndex,indexOf($minterm));//add to list of all indexes
    }
    
    $fullIndex=array_unique($fullIndex);//remove duplicate values in list of indexes
    
    //check to see if viables 1-index comparisons
    foreach($fullIndex as $index)
    {
      if(in_array($index-1,$fullIndex))//if the array contains the index 1 lower
      {
        array_push($viables, array($index-1,$index));//the comparison is viable
      }
    }
    return $viables;//return the array of viable comparions
                    //the first element of each array is the smaller index
                    //the 2nd element of each array is the bigger index
  }

  function testMin($minterms,$varlist)
  {
    $viables=viableIndexes($minterms);
    $changed=true;
    
    while($changed)
    {
      $index=indexMinterms($minterms);
      $changed=false;
      foreach($viables as $val)
      {
        $bigIndex=$val[1];
        $smallIndex=$val[0];
        foreach($index[$bigIndex] as $big)
        {
          foreach($index[$smallIndex] as $small)
          {
            if(differbyOne($big->array,$small->array))
            {
              $new=new Minterm($big,$small);
              array_push($minterms, $new);
              $big->covered=true;
              $small->covered=true;
              $changed=true;
            }
          }
        }
      }
      foreach($minterms as $key=>$minterm)
      {
        if($minterm->covered)
        {
          unset($minterms[$key]);
        }
      }
    }
    
    $minterms=uniqueTerms($minterms,$varlist);
    return $minterms;
  }

  function noConflict($term,$minterm)
  {
    $size=count($term->array);
    for($i=0;$i<$size;$i++)
    {
      if($minterm->array[$i]==='x')
      {
        //do nothing;
      }
      elseif($minterm->array[$i]===$term->array[$i])
      {
        //also do nothing;
      }
      elseif($minterm->array[$i]!==$term->array[$i])
      {
        return false;
      }
    }
    return true;
  }

  function setCovers($terms,$minterms)
  {
    foreach($terms as $term)
    {
      foreach($minterms as $minterm)
      {
        if(noConflict($term,$minterm))
        {
          array_push($term->covered,$minterm);
          array_push($minterm->covers,$term);
        }
      }
    }
  }

  function doesContain($term,$array,$list)
  {
    $needle=$term->toString($list);
    echo $needle;
    
    foreach($array as $element)
    {
      $haystack=$element->toString($list);
      if($needle===$haystack)
      {
        return true;
      }
    }
    return false;
  }

  function createSolutions($minterms,$list)
  {
    $size=count($minterms[0]->array);
    
    $solutions=array();//all final possible solution combinations
    
    for($i=0;$i<$size;$i++)
    {
      foreach($minterms as $m1)
      {
        $current=array($m1);
        
        for($j=1;$j<$i;$j++)
        {
          foreach($minterms as $m2)
          {
            if(doesContain($m2,$current,$list))
            {
              $solution = new Solution($current,$list);
              array_push($solutions,$solution);
            }
            else
            {
              array_push($current,$m2);
              $solution = new Solution($current,$list);
              array_push($solutions,$solution);
            }
          }
        }
      }
    }
    return $solutions;
  }

  //reduce the list of functions to those that actually cover all terms
  function findCoveringSolutions($solutions,$terms,$list)
  {
    $covering=array();//create a blank array for solutions that cover all remaining terms
    
    foreach($solutions as $solution)
    {
      if($solution->coversAll($terms,$list))
      {
        array_push($covering,$solution);
      }
    }
    return $covering;
  }
      
  //find the solution with the lowest number of literals, lowest number of terms acts as tiebreaker
  function bestSolution($solutions)
  {
    $minLiterals=$solutions[0]->literals;
    $minTerms=$solutions[0]->terms;
    $index=0;//index of current best solution
    
    foreach($solutions as $key=>$solution)
    {
      if( $solution->literals< $minLiterals )
      {
        $minLiterals=$solution->literals;
        $minTerms=$solution->terms;
        $index=$key;
      }
      elseif(($solution->literals===$minLiterals) && ($solution->terms<$minTerms))
      {
        $minLiterals=$solution->literals;
        $minTerms=$solution->terms;
        $index=$key;
      }
    }
    return $solutions[$index];
  }
   
  class Solution
  {
    public $literals;//the number of literals in the expression
    public $terms;//the number of terms in the expression, acting as a tiebreaker criteria
    public $string;//the string representation of the solution
    public $array;//the array of composing minterms
    
    function __construct($minterms, $list)
    {
      $this->string="";//create blank string representation
      $this->literals=0;//set the literals variable
      $this->terms=0;//set the terms variable
      $this->array=array();//create a blank array to contain the minterms
      
      //set the class instance variables 
      foreach($minterms as $minterm)
      {
        $this->string.=$minterm->toString($list);//add to the string representation
        $this->literals+=$minterm->literals();//increment the literals variable
        $this->terms+=1;//increment the terms variable
        array_push($this->array,$minterm);//add each minterm to the array variable
      }
      $this->string[0]="";//remove the 1st + that comes with the toString function
    }

    function coversAll($terms,$list)
    {
      $covered=array();
      
      foreach($this->array as $minterm)
      {
        foreach($minterm->covers as $term)
        {
          array_push($covered,$term);
        }
      }
      
      $covered=uniqueTerms($covered,$list);
      
      if(count($covered)===count($terms))
      {
        return true;
      }
      //else
      return false;
    }       
  }

  //TESTING

  //function to determine if a minterm can be made from the given set of unique terms
  //@param $terms: the unique set of terms for the function
  function mintermPossible($terms)
  {
    //check every single term against every term that is not itself, for minterms
    foreach($terms as $key1=>$term1)
    {
      foreach($terms as $key2=>$term2)
      {
        if($key1===$key2)//if checking against same term
        {
          //do nothing
        }
        else//not same term
        {
          if(differByOne($term1->array,$term2->array))//minterm can be made
          {
            return true;//a minterm is possible
          }
          //else nothing
        }
      }
    }
    return false;//can only get here if no minterms possible
  }

  //the term that represents an exclusive-or series, where an even number of the 
  //makeup terms can be true to set the entire term to true, all else is false
  class XOR_class
  {
    public $array;//the array that represents the XOR term
    public $trues;//the list of term->array values from terms this XOR covers
      
    //function __construct  
  }

  function checkForXor($terms)
  {

  }

  function simplify($expr)
  {
    $expr=fixFormat($expr);
    //echo "FIXED FORMAT:".$expr."\n\n";
    
    $list=varlist($expr);
    //echo"VARLIST\n";
    //var_dump($list);
    //echo"\n\n";
    
    $terms=createTerms($expr);
    
    $terms=uniqueTerms($terms,varlist($expr));
    
    //echo"TERMS";
    //var_dump($terms);
    //echo"\n\n";
    
    //$xors=new array();//array for XOR terms
    
    if(!mintermPossible($terms))
    {
      return $expr;
    }
    //else continue as normal
    
    $minterms=createMinterms($terms);//create all possilbe minterms
    $minterms=uniqueTerms($minterms,varlist($expr));
    
    $minterms=testMin($minterms,$list);//minimize the minterms using the QM algorithm
    
    setCovers($terms,$minterms);//set the covering/covered terms and minterms for each
    $essentials=array();//create blank array for essential prime implicants
    $notCovered=array();//create array for terms that are not covered by minterms
    
    //remove all uncovered terms from the list of terms
    //add them to their own array
    
    //remove all essential PI from minterms
    //remove all terms covered by the EPI from $terms
    //add all EPIs to their own array
    
    //remove all DC terms from terms
    foreach($terms as $key=>$term)
    {
      if(!($term->care))//the term is a don't-care
      {
        unset($terms[$key]);
      }
      
      if(!($term->isCovered()))//the term is not covered by a minterm
      {
        array_push($notCovered,$term);
        unset($terms[$key]);
      }
      
      if(count($term->covered)===1)//the term is covered by an essential prime implicant
      {
        array_push($essentials,$term->covered[0]);//push the essential PI on to $essentials
        unset($terms[$key]);
      }
    }        

    //unset all terms from $terms that are covered by the prime implicant 
    foreach($essentials as $minterm)
    {
      foreach($terms as $key=>$term)
      {
        if(noConflict($term,$minterm))
        {
          unset($terms[$key]);
          //echo"GOT RID OF TERM COVERED BY EPI:".$term->toString($list)."\n";
        }
      }
    }
    
    //unset all the EPI from $minterms
    foreach($minterms as $key=>$minterm)
    {
      foreach($essentials as $essential)
      {
        if($essential->array===$minterm->array)
        {
          unset($minterms[$key]);
        }
      }
    }
     
    $minterms=uniqueTerms($minterms,$list);
    if(count($minterms)===0)
    {
      //do nothing,create no solutions
    }
    else
    {
     $solutions=createSolutions($minterms,$list);
     
     //echo count($solutions[1]->array);
     //echo $solutions[01]->coversAll($terms,$list);
     
     //var_dump($solutions[1]->array[1]);
     
     $solutions=findCoveringSolutions($solutions,$terms,$list);
     
     $solution=bestSolution($solutions);
    }
    
    $essentials=uniqueTerms($essentials,$list);
    $notCovered=uniqueTerms($notCovered,$list);
    
    $final="";
    
    foreach($essentials as $essential)
    {
      $final.=$essential->toString($list);
      echo $essential->toString($list)."\n";
    }
    
    $final[0]="";
    $final=str_replace(" ","",$final);
    $final=str_replace("+"," + ",$final);
    return $final;
  }

?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Simplify</title>
  </head>

  <body>
    <?php include("includes/navbar.html");?>

    <div class="container-fluid">

      <div class="row">

        <div class="col-10 col-sm-10 col-lg-10">
          <form method="post" action="<?php echo $PHP_SELF;?>">
            <textarea class="form-control" name="expr" rows="10" data-toggle="tooltip" data-placement="top" title="Your expression"><?php if (isset($_POST['submit'])){echo $expr;}?></textarea><br />
            <button type="submit" value="submit" name="submit" class="btn btn-default">Submit</button><br /><br />
            <div class="well well-lg" name="output" data-toggle="tooltip" data-placement="top" title="Your simplified expression"><?php if (isset($_POST['submit'])){echo simplify($expr);}?></div>
          </form>
        </div> <!-- col -->

        <div class="col-2 col-sm-2 col-lg-2">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
            <span class="glyphicon glyphicon-question-sign"></span>
          </button>

          <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Help</h4>
                </div>
                <div class="modal-body">
                  <p>Enter an expression in the input box, then press the go button. The simplified result of the equation, based on the Quine-McClusky algorithm is shown in the output box.Formatting for expressions allows all standardized notations for logical operators.</p>
                  <p>ANDs can be expressed with AND,And,.,and</p>
				          <p>ORS can be expressed with +, or, OR, Or</p>
				          <p>XORS can be expressed by XOR, Xor, xor,</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!--div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <div class="input-group-btn">
              <input type="text" class="form-control">
              <button type="button" class="btn btn-default" tabindex="-1">Go</button>
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">XOR</a></li>
                <li><a href="#">Something</a></li>
                <li><a href="#">Something else</a></li>
              </ul>
            </div>
          </div><!-- /.input-group
        </div><!-- /.col-lg-6
      </div-->

      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script type="text/javascript">
      $('.tooltip').tooltip();
    </script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
