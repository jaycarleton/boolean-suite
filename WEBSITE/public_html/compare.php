<!DOCTYPE html>

<?php
  $expr1 = $_POST["expr1"];
  $expr2 = $_POST["expr2"];

  //CODE FOR COMPARE EXPRESSIONS PAGE
  function inc($a)//where a is an array
  {
    $length=count($a);
    //assume not given all 1's
    if($a[$length-1]==0)
    {
      $a[$length-1]=1;
      //echo "last is zero"; testing only
      return $a;
    }
    //else
    $i=$length-1;
    while($a[$i]==1)
    {
      $a[$i]=0;
      $i--;
    }
    if($a[$i]==0)
    {
      $a[$i]=1;
    }
    return $a;
  }

  //Standardizes the various possible input notations of a user to php understand boolean syntax
  function fixformat($expr)
  {
    //Fix the XORS
    $xor=array('XOR','Xor ','xor');
    $expr=str_replace($xor,'^',$expr);
    
    //Fix the ORs
    $or=array('+','or','OR','Or');
    $expr=str_replace($or,'||',$expr);
    
    //Fix the ANDS
    $and=array('.','AND','and','And');
    $expr=str_replace($and,'&&',$expr);
    
    //Fix the parenthases
    $openparen=array('[','{');
    $closeparen=array(']','}');
    $expr=str_replace($openparen,'(',$expr);
    $expr=str_replace($closeparen,')',$expr);
    
    //Fix the Spaces
    $expr=str_replace(' ','',$expr);
    
    //Echo just to check the above "fixing" of input types worked
    //echo $expr; testing only
    return $expr;
  }

  //Function to remove all multiple spaces with single spaces so the varlist function has no empty elements
  //Recursion could be used, however this is faster for the server to process
  function single($string)
  {
    $length=strlen($string);//start off at max possible value
    for($i=$length;$i>1;$i--)//decrement each time
    {
      if(strpos($string,str_repeat(' ', $i))!==false)//if X number of spaces exist                                 concurrently
      {
        //echo $string."\n"; for testing only
        $string=str_replace( (str_repeat(' ',$i)),' ',$string);//replace the X spaces with a single space
      }
    }
    return $string;
  }

  //Function to creat an array of individual variables inside the expression, one of each element only
  function varlist($expr)
  {
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('||','&&','^',')','(','[',']','\n','\t');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    
    $stack = explode(' ', $i);
    //remove duplicates
    $stack= array_unique($stack);
    //var_dump($stack); for testing only
    foreach($stack as $key=>$value)
    {
      if ($value=="")
      {
        unset($stack[$key]);
      }
    }
    $x=array();
    foreach($stack as $value)
    {
      array_push($x,$value);
    }
    
    return $x;//returns array of variables 
  }

  //a list of all variables for both expressions, assumes already gotten correct varlist from both
  function masterlist($list1,$list2)
  {
    if (count($list1)>count($list2))
    {
      $master=$list1;
      $slave=$list2;
    }
    else
    {
      $master=$list2;
      $slave=$list1;
    }
    
    foreach($slave as $value)
    {
      if (!in_array($value, $master)) 
      {
        array_push($master,$value);
      }
    }
    return $master;
  }

  function ismax($i)//test for 111 at end of loop
  {
    foreach($i as $value)
    {
      if($value===0)
      {
        return false;
      }
    }
    return true;
  }

  //Function evaluates the boolean result of expression where the given list of parameters is true
  function result($expr,$trues)
  {    
    $e=str_replace($trues,'true',$expr);
    $e=str_replace(varlist($expr),'false',$e);
    //echo $e;//for testing only
    
    //Eval
    eval("\$result=$e;");
    return $result;
  }

  function testall($expr1,$expr2,$list)
  {
    $i=array();
    foreach($list as $value)//create empty array of size varlist
    {
      array_push($i,0);
    }
  	while(true)
  	{
      //var_dump($i);//testing only
      $trues=array();//clear array
      foreach($i as $key=>$value)//create array of trues based on current i list
      {
        if ($value===1)
        {
          array_push($trues,$list[$key]);
        }
      }
      if (result($expr1,$trues)!=result($expr2,$trues))
      {
        return false;//not same expression
      }
      
      //else
      if(ismax($i))//we got to the last one and none of them are different
      {
        return true;//same expression
      }
      $i=inc($i);
      
    }
  }
     
  // function isWellFormed($expr1, $expr2, $list)
  // {
  // 	//TODO: implement
  // }

?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Compare</title>
  </head>

  <body>
  	<?php include("includes/navbar.html");?>

  	<div class="container-fluid">
  		<div class="row">

        <div class="col-10 col-sm-10 col-lg-10">
        	<form method="post" action="<?php echo $PHP_SELF;?>">
          	<textarea class="form-control" name="expr1" rows="10" placeholder="Expression 1"><?php if (isset($_POST['submit'])){echo $expr1;}?></textarea><br />
          	<textarea class="form-control" name="expr2" rows="10" placeholder="Expression 2"><?php if (isset($_POST['submit'])){echo $expr2;}?></textarea><br />
          	<button type="submit" value="submit" name="submit" class="btn btn-default">Submit</button><br />
          </form>
        </div> <!-- col -->

        <div class="col-2 col-sm-2 col-lg-2">

          <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
            <span class="glyphicon glyphicon-question-sign"></span>
          </button>

          <br />

          <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Help</h4>
                </div>
                <div class="modal-body">
                  <p>Enter two different boolean/digital equations in the different boxes, then press GO, then the result of the comparison is shown in the box below. Formatting for expressions allows all standardized notations for logical operators.</p>
                  <p>ANDs can be expressed with AND,And,.,and</p>
  			  				<p>ORS can be expressed with +, or, OR, Or
  			  				<p>XORS can be expressed by XOR, Xor, xor,</p>
  							</div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <div class="row-fluid">
          	<h3>Results:</h3>
          	<div class="well">
            	<?php
  							if (isset($_POST["submit"])) {
  								//$expr1="(a.b+a)orc";
  								//$expr2="a+c";
  								$expr1=fixformat($expr1);
  								$expr2=fixformat($expr2);
  								//echo $expr1."\n";
  								//echo $expr2."\n";

  								$list1=varlist($expr1);
  								//var_dump($list1);
  								$list2=varlist($expr2);
  								//var_dump($list2);

  								$master=masterlist($list1,$list2);
  								//var_dump($master);

  								if($expr1=="" or $expr2=="") {
  								?>
  							    <img class="img-responsive" src="/images/uncertain.png">
  								<?php 
  								} else if(testall($expr1,$expr2,$master)){ 
  								?>
  							    <img class="img-responsive" src="/images/true.png">
  								<?php 
  								} else {
  								?>
  							    <img class="img-responsive" src="/images/false.png">
  								<?php
  								}
  							}
  						?>
          	</div>
          </div> <!-- row-fluid -->

        </div> <!-- col -->

      </div> <!-- row -->
  	</div><!-- container-fluid -->

  	<hr>

    <footer>
      <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
    </footer>
  	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>