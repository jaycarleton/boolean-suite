<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Home</title>
  </head>

  <body>
    <?php include("includes/navbar.html");?>

    <div class="container-fluid">
    	<h2>A suite of web tools used for boolean logic simplification</h2>
      <div class="row">
      	<div class="col-2 col-sm-2 col-lg-2">
      	</div>
        <div class="col-4 col-sm-4 col-lg-4" style="border: 1px solid black">
          <h2>Simplify</h2>
          <p>Description.</p>
          <p><a class="btn btn-default" href="simplify.php" role="button">Go &raquo;</a></p>
        </div>
        <div class="col-4 col-sm-4 col-lg-4" style="border: 1px solid black">
          <h2>Unavailable</h2>
          <p>Description.</p>
          <p><a class="btn btn-default" href="#" role="button">Go &raquo;</a></p>
        </div>
        <div class="col-2 col-sm-2 col-lg-2">
        </div>
      </div><!--/row-->

      <div class="row">
        <div class="col-2 col-sm-2 col-lg-2"></div>
        <div class="col-4 col-sm-4 col-lg-4" style="border: 1px solid black">
          <h2>Unavailable</h2>
          <p>Description.</p>
          <p><a class="btn btn-default" href="#" role="button">Go &raquo;</a></p>
        </div>
        <div class="col-4 col-sm-4 col-lg-4" style="border: 1px solid black">
          <h2>Unavailable</h2>
          <p>Description.</p>
          <p><a class="btn btn-default" href="#" role="button">Go &raquo;</a></p>
        </div>
        <div class="col-2 col-sm-2 col-lg-2"></div>
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>
