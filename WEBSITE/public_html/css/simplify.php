<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Simplify</title>
  </head>

  <body>
    <!--?php include("includes/navbar.html");?-->

    <div class="container-fluid">

      <div class="row">

        <div class="col-10 col-sm-10 col-lg-10">
          <textarea class="form-control" rows="20"></textarea>
        </div>

        <div class="col-2 col-sm-2 col-lg-2">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
            <span class="glyphicon glyphicon-question-sign"></span>
          </button>

          <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Help</h4>
                </div>
                <div class="modal-body">
                  <p>You're on your own buddy!</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <div class="input-group-btn">
              <input type="text" class="form-control">
              <button type="button" class="btn btn-default" tabindex="-1">Go</button>
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">XOR</a></li>
                <li><a href="#">Something</a></li>
                <li><a href="#">Something else</a></li>
              </ul>
            </div>
          </div><!-- /.input-group -->
        </div><!-- /.col-lg-6 -->
      </div>

      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
