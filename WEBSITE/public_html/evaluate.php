<!DOCTYPE html>

<?php
	$expr = $_POST["expr"];
	$list = $_POST["list"];

  //Standardizes the various possible input notations of a user to php understand boolean syntax
  //@param $expr: the expression to standardize
  //@retun $expr: the newly standardized expression
  function fixformat($expr)
  {
    //Fix the XORS
    $xor=array('XOR','Xor ','xor');
    $expr=str_replace($xor,'^',$expr);
    
    //Fix the ORs
    $or=array('+','or','OR','Or');
    $expr=str_replace($or,'||',$expr);
    
    //Fix the ANDS
    $and=array('.','AND','and','And');
    $expr=str_replace($and,'&&',$expr);
    
    //Fix the parenthases
    $expr=str_replace('[','(',$expr);
    $expr=str_replace(']',')',$expr);
    
    //Fix the Spaces
    $expr=str_replace(' ','',$expr);

    return $expr;//return the standardized expression
  }


  //Function to replace all multiple spaces with single spaces so the varlist function has no empty elements
  //Recursion could be used, however this is faster for the server to process
  //@param $string: the string that may contain multiple spaces
  //@return $string: the newly modified string, with no duplicate spaces
  function single($string)
  {
    $length=strlen($string);//start off at max possible value
    for($i=$length;$i>1;$i--)//decrement each time
    {
      if(strpos($string,str_repeat(' ', $i))!==false)//if X number of spaces exist                                 concurrently
      {
        //echo $string."\n"; for testing only
        $string=str_replace( (str_repeat(' ',$i)),' ',$string);//replace the X spaces with a single space
      }
    }
    return $string;//return the modified string, with no multiple spaces
  }



  //Function to creat an array of individual variables inside the expression, one of each element only
  //@param $expr: the expression to create a list of variables for
  //@return $x: the list of unique variables in the expression
  function varlist($expr)
  {
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('||','&&','^',')','(','[',']','\n','\t','!');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    $stack = explode(' ', $i);//divide string into array, seperate at each space
    
    //remove duplicates
    $stack= array_unique($stack);//make array unique
    foreach($stack as $key=>$value)//remove empty element entries
    {
      if ($value=="")//if element is blank
      {
        unset($stack[$key]);//remove from list
      }
    }
    
    $x=array();//create blank array for answer list
    
    //fix the indices of the list by copying the current one, element-by-element
    foreach($stack as $value)
    {
      array_push($x,$value);//push onto new array
    }
    
    return $x;//returns array of variables 
  }

  //Creates an arraylist of values that are set true manually by the user
  //@param $expression: the expression for which to find true variables for
  //@return $stack: the array of true variables
  function findtrues($expr)
  {
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('.',',',';',' ');
      
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
      
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
      
    //Creates array of variables, no empties, may have duplicates
    $stack = explode(' ', $i);
      
    //remove duplicates
    $stack= array_unique($stack);
    //var_dump($stack); for testing only
              
    return $stack;//returns array of variables 
  }



  //Function evaluates the boolean result of expression where the given list of parameters is true
  //@param $expr: the expression to evaluate
  //@param $trues: the list of variables set true in the expression evaluation
  function result($expr,$trues)
  {   
    //replace the variables with their literal boolean values
    $e=str_replace($trues,'true',$expr);//replace the trues
                                          //variables no longer exist in expression
                                          //only false remain
    $e=str_replace(varlist($expr),'false',$e);//all else set false
      
    //Eval
    eval("\$result=$e;");//evaluate string as boolean expression
    return $result;//return boolean result of expression
  }

  //function to find the output of a boolean expression, given a list of true variables
  //@param $expr: the expression to evaluate
  //@param $list: the list of variables that are set to true for the expression
  //@return $output: the boolean value of the expression, when certain variables are set true
  function evaluate($expr,$list)
  {
    $expr=fixformat($expr);//standardize format of expression
    
    $trues=findtrues($list);//create standardized list of true variables based on list
    
    $output=result($expr, $trues);//evaluate the result of the expression
    
    return $output;//return the final value of the expression
  }
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Evaluate</title>
  </head>

  <body>
    <?php include("includes/navbar.html");?>

    <div class="container-fluid">

      <div class="row">
        <div class="col-10 col-sm-10 col-lg-10">
          <form method="post" action="<?php echo $PHP_SELF;?>">
            <textarea class="form-control" name="expr" rows="10" placeholder="Expression"><?php if (isset($_POST['submit'])){echo $expr;}?></textarea><br />
            <input type="text" name="list" class="form-control" placeholder="List of true variables"><?php if (isset($_POST['submit'])){echo $list;}?></input><br />
            <button type="submit" value="submit" name="submit" class="btn btn-default">Submit</button><br />
          </form>
        </div>

        <div class="col-2 col-sm-2 col-lg-2">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
            <span class="glyphicon glyphicon-question-sign"></span>
          </button>

          <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Help</h4>
                </div>
                <div class="modal-body">
                  <p>Enter an expression in the first input box, then enter a list of variable names in the expression that are set to be true. The variable names must correspond to those in the entered equation. All other variables in the expression will be evaluated as false by default. The variables should be seperated by any number/combination of commas, semi-colons, periods, and spaces. Formatting for expressions allows all standardized notations for logical operators.</p>
                  <p>ANDs can be expressed with AND,And,.,and</p>
            		  <p>ORS can be expressed with +, or, OR, Or</p>
            		  <p>XORS can be expressed by XOR, Xor, xor,</p></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <div class="row-fluid">
            <h3>Results:</h3>
            <div class="well">
              <?php
                if (!isset($_POST["submit"])) { // if page is not submitted to itself echo nothing
                ?>

                <?php
                } 
                else {
                  if($expr=="" or $list=="") {
                  ?>
                      <img class="img-responsive" src="/images/uncertain.png">
                  <?php 
                  } else if(evaluate($expr,$list)){ 
                  ?>
                      <img class="img-responsive" src="/images/true.png">
                  <?php 
                  } else {
                  ?>
                      <img class="img-responsive" src="/images/false.png">
                  <?php
                  }
                }
              ?>
            </div>
          </div> <!-- row-fluid -->
        </div>

      </div> <!-- row -->

      <hr>

      <footer>
        <p>&copy; Josh Cohen-Collier & Brandon To 2014</p>
      </footer>

    </div><!--/.container-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>
